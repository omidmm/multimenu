<?php namespace Omidmm\Multimenu\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Omidmm\Multimenu\Models\Menu;

/**
 * Channels Back-end Controller
 */
class Menus extends Controller
{
	public $implement = [
		'Backend.Behaviors.FormController',
		'Backend.Behaviors.ListController'
	];

	public $formConfig = 'config_form.yaml';
	public $listConfig = 'config_list.yaml';

	public $requiredPermissions = ['omidmm.multimenu.access_menus'];

	public function __construct()
	{
		parent::__construct();

		BackendMenu::setContext('omidmm.multimenu', 'menu', 'menus');
		$this->addCss('/plugins/omidmm/multimenu/assets/css/admin.css');
	}
}