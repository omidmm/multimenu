<?php namespace Omidmm\Multimenu;

use Backend;
use System\Classes\PluginBase;
use System\Classes\PluginManager;

/**
 * Menus Plugin Information File
 */
class Plugin extends PluginBase
{

	/**
	 * Returns information about this plugin.
	 *
	 * @return array
	 */
	public function pluginDetails()
	{
		return [
			'name'        => 'Multi Menu',
			'description' => 'Create flexible menus straight from October CMS admin',
			'author'      => 'Flyn San',
			'icon'        => 'icon-bars',
            'homepage'    => 'https://github.com/omidmm/oc-menu-plugin'
		];
	}

	public function registerNavigation()
	{
		return [
			'menu' => [
				'label'       => 'Menus',
				'url'         => Backend::url('omidmm/multimenu/menus'),
				'icon'        => 'icon-bars',
				'permissions' => ['omidmm.multimenu.*'],
				'order'       => 500,

				'sidsMenu' => [
					'menus' => [
						'label'       => 'All Menus',
						'icon'        => 'icon-bars',
						'url'         => Backend::url('omidmm/multimenu/menus'),
						'permissions' => ['omidmm.multimenu.access_menus'],
					],
					'settings' => [
						'label'       => 'Settings',
						'icon'        => 'icon-cog',
						'url'         => Backend::url('omidmm/multimenu/settings'),
						'permissions' => ['omidmm.multimenu.access_menu_settings'],
					],
				]

			]
		];
	}

	public function registerPermissions()
	{
		return [
			'omidmm.multimenu.access_menus'          => ['label' => 'Menus - Access Menus', 'tab' => 'multimenu'],
			'omidmm.multimenu.access_menu_settings'  => ['label' => 'Menus - Access Settings', 'tab' => 'multimenu'],
		];
	}

	public function registerComponents()
	{
		return [
			'\Omidmm\Multimenu\Components\Menu' => 'menu'
		];
	}

	public function register_omidmm_menu_item_types()
	{
		$types = [
			'Omidmm\\Multimenu\\MenuItemTypes\\Page' => [
				'label' => 'Page',
				'alias' => 'page',
				'description' => 'A link to a CMS Page'
			],
			'Omidmm\\Multimenu\\MenuItemTypes\\Partial' => [
				'label' => 'Partial',
				'alias' => 'partial',
				'description' => 'Render a CMS Partial'
			],
			'Omidmm\\Multimenu\\MenuItemTypes\\Link' => [
				'label' => 'Link',
				'alias' => 'link',
				'description' => 'A given URL'
			],
		];

		if ( PluginManager::instance()->hasPlugin('RainLab.Blog') )
		{
			$types['Omidmm\\Multimenu\\MenuItemTypes\\BlogPost'] = [
				'label' => 'Blog Post',
				'alias' => 'blog_post',
				'description' => 'A link to a Blog Post',
			];

			$types['Omidmm\\Multimenu\\MenuItemTypes\\BlogCategory'] = [
				'label' => 'Blog Category',
				'alias' => 'blog_category',
				'description' => 'A link to a Blog Category'
			];
		}

		return $types;
	}
}
