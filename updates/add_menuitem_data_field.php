<?php namespace Omidmm\Multimenu\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddMenuitemDataField extends Migration
{

	public function up()
	{
		Schema::table('omidmm_menu_menuitems', function($table)
		{
			$table->text('data')->nullable();
		});
	}

	public function down()
	{
		Schema::table('omidmm_menu_menuitems', function($table)
		{
			$table->dropColumn('data');
		});
	}

}
